import unittest
from app.features.geohash import get_geohash


class GeohashTest(unittest.TestCase):
    def test_get_geohash_success(self):
        get_geohash(51.3234, 45.34, 5)
        self.assertEqual('v105z', get_geohash(51.3234, 45.34, 5))

    def test_get_geohash_exception(self):
        with self.assertRaises(Exception): get_geohash(51.3234, 'incorrect', 5)


if __name__ == '__main__':
    unittest.main()

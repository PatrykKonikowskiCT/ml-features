from app import app
from app.features.geohash import get_geohash
from flask import request, jsonify
import os, traceback
from pprint import pprint


@app.route('/geohash/')
def geohash():
    lat = request.args.get('lat', default=None, type=float)
    lng = request.args.get('lng', default=None, type=float)
    precision = request.args.get('precision', default=5, type=int)

    return get_geohash(lat, lng, precision)


@app.errorhandler(Exception)
def handle_exception(e):
    error_message = str(e)
    error_message += traceback.format_exc() if os.getenv('FLASK_ENV') == 'development' else ''

    return jsonify(
        error=error_message,
        status=400
    ), 400

import geohash2


def get_geohash(lat, lng, precision):
    return geohash2.encode(lat, lng, precision)
